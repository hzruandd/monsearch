# -*- coding: utf-8 -*-

from tornadoes.models import BulkList

from six.moves.urllib.parse import urlencode
from tornado.escape import json_encode, json_decode
from tornado.ioloop import IOLoop
from tornado.httpclient import AsyncHTTPClient, HTTPRequest
from tornado.concurrent import return_future


class ESConnection(object):

    def __init__(self, host='localhost', port='9200', io_loop=None, protocol='http'):
        self.io_loop = io_loop or IOLoop.instance()
        self.url = "%(protocol)s://%(host)s:%(port)s" % {"protocol": protocol, "host": host, "port": port}
        self.bulk = BulkList()
        self.client = AsyncHTTPClient(self.io_loop)
        self.httprequest_kwargs = {}     # extra kwargs passed to tornado's HTTPRequest class e.g. request_timeout

    def create_path(self, method, **kwargs):
        index = kwargs.pop('index', '_all')
        doc_type = '/%s' % kwargs.pop('type', '')

        parameters = {}
        for param, value in kwargs.items():
            parameters[param] = value

        path = "/%(index)s%(type)s/_%(method)s" % {
            "method": method,
            "index": index,
            "type": doc_type
        }
        if parameters:
            path += '?' + urlencode(parameters)

        return path

    @return_future
    def search(self, callback, **kwargs):
        """
        通常，GET请求将返回文档的全部，存储在_source参数中。但是可能你感兴趣的字段只是title。请求个别字段可以使用_source参数。多个字段可以使用逗号分隔：

        GET /website/blog/123?_source=title,text
        _source字段现在只包含我们请求的字段，而且过滤了date字段：
        {
        "_index" :   "website",
        "_type" :    "blog",
        "_id" :      "123",
        "_version" : 1,
        "exists" :   true,
        "_source" : {
        "title": "My first blog entry" ,
        "text":  "Just trying this out..."
        }
        }

        或者你只想得到_source字段而不要其他的元数据，你可以这样请求：
        GET /website/blog/123/_source
        它仅仅返回:
            {
            "title": "My first blog entry",
            "text":  "Just trying this out...",
            "date":  "2014/01/01"
            }
        """
        path = self.create_path("search", **kwargs)
        source = json_encode(kwargs.get('source', {
            "query": {
                "match_all": {}
            }
        }))
        self.post_by_path(path, callback, source)

    def multi_search(self, index, source):
        self.bulk.add(index, source)

    @return_future
    def apply_search(self, callback, params={}):
        path = "/_msearch"
        if params:
            path = "%s?%s" % (path, urlencode(params))
        source = self.bulk.prepare_search()
        self.post_by_path(path, callback, source=source)

    def post_by_path(self, path, callback, source):
        url = '%(url)s%(path)s' % {"url": self.url, "path": path}
        request_http = HTTPRequest(url, method="POST", body=source, **self.httprequest_kwargs)
        self.client.fetch(request=request_http, callback=callback)

    @return_future
    def get_by_path(self, path, callback):
        url = '%(url)s%(path)s' % {"url": self.url, "path": path}
        self.client.fetch(url, callback, **self.httprequest_kwargs)

    @return_future
    def get(self, index, type, uid, callback, parameters=None):
        def to_dict_callback(response):
            source = json_decode(response.body)
            callback(source)
        self.request_document(index, type, uid, callback=to_dict_callback, parameters=parameters)

    @return_future
    def put(self, index, type, uid, contents, parameters=None, callback=None):
        """
        插入和更新都使用此接口。
        如果请求成功的创建了一个新文档，Elasticsearch将返回正常的元数据且响应状态码是201 Created。
        如果包含相同的_index、_type和_id的文档已经存在，Elasticsearch将返回409 Conflict响应状态码。
        """
        self.request_document(
            index, type, uid, "PUT", body=json_encode(contents),
            parameters=parameters, callback=callback)

    @return_future
    def delete(self, index, type, uid, parameters=None, callback=None):
        self.request_document(index, type, uid, "DELETE", parameters=parameters, callback=callback)

    @return_future
    def count(self, index="_all", type=None, source='', parameters=None, callback=None):
        path = '/{}'.format(index)

        if type:
            path += '/{}'.format(type)

        path += '/_count'

        if parameters:
            path += '?{}'.format(urlencode(parameters or {}))

        if source:
            source = json_encode(source)

        self.post_by_path(path=path, callback=callback, source=source)

    def request_document(self, index, type, uid, method="GET", body=None, parameters=None, callback=None):
        path = '/{index}/{type}/{uid}'.format(**locals())
        if method != "GET":
            url = '%(url)s%(path)s' % {
                "url": self.url,
                "path": path
            }
        else:
            url = '%(url)s%(path)s?%(querystring)s' % {
                "url": self.url,
                "path": path,
                "querystring": urlencode(parameters or {})
            }
        request_arguments = dict(self.httprequest_kwargs)
        request_arguments['method'] = method

        if body is not None:
            request_arguments['body'] = body

        request = HTTPRequest(url, **request_arguments)
        return self.client.fetch(request, callback)

    def validate_query(self, index, _type, body, callback):
        url = "%s/%s/_validate/query?explain" % (index, _type)
        request_arguments = dict(self.httprequest_kwargs)
        request_arguments['method'] = "GET"

        if body is not None:
            request_arguments['body'] = body

        request = HTTPRequest(url, **request_arguments)
        self.client.fetch(request, callback)
