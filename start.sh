/usr/bin/mongod --dbpath /var/lib/mongodb/ --logpath /var/log/mongodb/mongodb.log --logappend &

/usr/lib/jvm/java-7-openjdk-i386//bin/java -Xms256m -Xmx1g -Xss256k
-Djava.awt.headless=true -XX:+UseParNewGC -XX:+UseConcMarkSweepGC
-XX:CMSInitiatingOccupancyFraction=75 -XX:+UseCMSInitiatingOccupancyOnly
-XX:+HeapDumpOnOutOfMemoryError -Delasticsearch
-Des.pidfile=/var/run/elasticsearch.pid -Des.path.home=/usr/share/elasticsearch
-cp
:/usr/share/elasticsearch/lib/elasticsearch-1.1.2.jar:/usr/share/elasticsearch/lib/*:/usr/share/elasticsearch/lib/sigar/*
-Des.default.config=/etc/elasticsearch/elasticsearch.yml
-Des.default.path.home=/usr/share/elasticsearch
-Des.default.path.logs=/var/log/elasticsearch
-Des.default.path.data=/var/lib/elasticsearch
-Des.default.path.work=/tmp/elasticsearch
-Des.default.path.conf=/etc/elasticsearch
org.elasticsearch.bootstrap.Elasticsearch

nohup /usr/local/kibana-4.1.2-linux-x86/bin/kibana >/dev/null &2>1 &
nohup /usr/local/elasticsearch-1.7.3/bin/elasticsearch >/dev/null &2>1 &
nohup  /opt/logstash/bin/logstash -f index_redis_ok.conf -f ship_redis_ok.conf >/dev/null &2>1 &
nohup /usr/share/elasticsearch/bin/elasticsearch >/dev/null &2>1 &
ps -ef|grep elasticsearch | grep -v grep|awk '{ print $2 }'|xargs kill -9
