#encoding:utf-8
import os

DICT_CONF = {
}

def read_conf(filename):
    if os.path.exists(filename):
        with open(filename) as fd:
            while 1:
                item = fd.readline()
                if item:
                    try:
                        k, v = item.split("=")
                        k = k.strip().replace("\n", "")
                        v = v.strip().replace("\n", "").split("\"")
                        if len(v) == 1:
                            v = v[0]
                        elif len(v) >=2:
                            v = v[1]
                        v = int(v)
                    except Exception as ex:
                        pass
                    DICT_CONF[k] = v
                    continue
                else:
                    break
    return DICT_CONF


if not DICT_CONF:
    read_conf("/etc/mon_conf.conf")
    print DICT_CONF


def get_conf(key):
    return DICT_CONF.get(key, None)


def set_conf(key, value):
    DICT_CONF.setdefault(key, vaule)


def reset_conf(key, value):
    DICT_CONF[key] = value

