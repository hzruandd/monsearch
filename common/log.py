import logging
import logging.handlers

from settings import LOG_FILE


def getlogger():
    handler = logging.handlers.RotatingFileHandler(LOG_FILE,
                                                   maxBytes=10*1024*1024,
                                                   backupCount=3)
    fmt = '%(asctime)s-%(filename)s:%(lineno)s-%(name)s-%(message)s'

    formatter = logging.Formatter(fmt)
    handler.setFormatter(formatter)

    logger = logging.getLogger('ElasticSearch')
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)
    return logger

LOG = getlogger()
