#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
import time

from pymysqlreplication import BinLogStreamReader
from pymysqlreplication.row_event import (
    DeleteRowsEvent,
    UpdateRowsEvent,
    WriteRowsEvent,
)

from common import pinyin
from common.pinyin import PinYin
from common.utils import do_request, is_alphabet
from common.log import LOG
from tornadoes import ESConnection

from common.db import fetchone, get_db_conn, execute, fetchall
from settings import DEBUG

from common.conf import DICT_CONF

ESPORT = DICT_CONF["ESPORT"]
ESHOST = DICT_CONF["ESHOST"]

MYSQL_SETTINGS = {
    "host": DICT_CONF["host"],
    "port": DICT_CONF["port"],
    "user": DICT_CONF["user"],
    "passwd": DICT_CONF["passwd"],
    "db": DICT_CONF["db"],
}

pinyin_obj = PinYin()
pinyin_obj.load_word()
CONN = get_db_conn()


class SyncES(object):
    es = ESConnection(ESHOST, ESPORT)

    stream = BinLogStreamReader(
        connection_settings=MYSQL_SETTINGS,
        server_id=1,
        only_events=[DeleteRowsEvent, WriteRowsEvent, UpdateRowsEvent],
        blocking=True, resume_stream=True,
        only_tables=["provider_goods", "auditing", "goods"],
        only_schemas=["floriation"],
    )

    def run(self):
        LOG.info("Start to sync data to ES.")
        print "Start to sync data to ES."
        for binlogevent in self.stream:
            indice, tipo = binlogevent.schema, binlogevent.table

            for row in binlogevent.rows:
                try:
                    if isinstance(binlogevent, DeleteRowsEvent):
                        body = self._get_params(row["values"], tipo)
                        _id = body['id']
                        indice, tipo = body["index"]
                        self.delete(indice, tipo, _id)
                    elif isinstance(binlogevent, UpdateRowsEvent):
                        body = self._get_params(row["before_values"], tipo)
                        _id = body['id']
                        sync_tag = "1" if row["before_values"]['sync_tag'] in [0, "0"] else "0"
                        indice, tipo = body["index"]
                        #NOTE(jake): deal with the soft deleted case here.
                        self.put(indice, tipo, _id, body)
                        LOG.debug("sync data to es: %s" % body)
                        try:
                            self._update_mysql_data(tipo, _id, sync_tag)
                        except Exception as ex:
                            pass
                    elif isinstance(binlogevent, WriteRowsEvent):
                        body = self._get_params(row["values"], tipo)
                        _id = body['id']
                        indice, tipo = body["index"]
                        self.put(indice, tipo, _id, body)
                        #r.hmset(prefix + str(vals["id"]), vals)
                except Exception as ex:
                    LOG.error("SyncES error:%s---%s" % (ex.args, ex.message))

    def _run_new(self):
        while True:
            self.sync_goods_to_es()
            self.sync_provider_goods_to_es()
            time.sleep(5)

    def sync_goods_to_es(self):
        goods_sql = '''
        select id, name, date, price, goods_category_id, sales, deleted, marketable, custom_property
        from goods where deleted = 0;
        '''
        global CONN
        goods = fetchall(CONN, goods_sql)
        for good in goods:
            body = self._get_params(goods, "goods")
            _id = body['id']
            indice, tipo = body["index"]
            self.put(indice, tipo, _id, body)

    def sync_provider_goods_to_es(self):
        provider_goods_sql = '''
        select id, name ,date, deleted, sales, provider_goods_category_id, create_time
        from provider_goods where deleted = 0;
        '''
        global CONN
        provider_goods = fetchall(CONN, provider_goods_sql)
        for provider_good in provider_goods:
            body = self._get_params(provider_good, "provider_goods")
            _id = body['id']
            indice, tipo = body["index"]
            self.put(indice, tipo, _id, body)


    def _update_mysql_data(self, table_name, _id, sync_tag):
        sql = "UPDATE %s SET sync_tag='%s' WHERE id=%s ;"
        param = (table_name, sync_tag, _id)
        global CONN
        return execute(CONN, sql, param)

    def put(self, indice, tipo, _id, body):
        #self.es.put(indice, tipo, _id, body)
        self._do_request("PUT", indice, tipo, _id, body)

    def delete(self, indice, tipo, _id):
        self._do_request("DELETE", indice, tipo, _id)

    def _do_request(self, method, indice, tipo, _id, body=None):
        result = {"status": 400}
        try:
            url = "/%s/%s/%s" % (indice, tipo, _id)
            result = do_request(method, url, body)
        except Exception as ex:
            message = ex.message if ex.message else ex.args[0]
            LOG.error("insert or update data to es error by: %s" % message)

        if int(result.get("status")) < 400:
            data = "%s--body(%s)" % (url, body)
            message = "action(%s) is ok, and data is: %s ." % (method, data)
            LOG.debug(message)

    def _get_params(self, vals, table):
        """
        处理监听表的增，删，改事件，数据格式化后存入ES引擎。
        """
        body = {}
        if table == "not_exist":
#        if table == "auditing":
            # status: 1,审核中；2，通过；3，失败；
            # 在ES添加商品时status都设置为3，即失败。只有审核通过的数据，才去ES引擎里做同步更新。
            if vals["status"] == "2":
                #0:商家发布的商品
                #1:供应商发布的商品
                body["id"] = vals["target_id"]
                if vals["target_type"] == "1":
                    body["index"] = ["floriation_online_provider_goods", "goods"]
                elif vals["target_type"] == "0":
                    body["index"] = ["floriation_online_goods", "goods"]

            return body
        elif table == "goods":
            body["index"] = ["floriation_online_goods", "goods"]
            body["status"] = "2"

            body["id"] = vals["id"]
            body["name"] = vals["name"]
            body['date'] = vals['create_time'].strftime("%Y-%m-%d %H:%M:%S")
            body["price"] = str(vals["current_price"])
            body["goods_category_id"] = vals["goods_category_id"]
            body["sales"] = vals.get("sales", 0)
            #由于binlog纪录的是update之前的数据，而不是把当前update的数据发送过来
            #所以，为了和数据库状态保持一致，需要取反操作，这样即可获取最新数据了。
#            if vals["deleted"] in [0, "0"]:
#                deleted = "1"
#            else:
#                deleted = "0"
#            if vals["marketable"] in [0, "0"]:
#                marketable = "1"
#            else:
#                marketable = "0"
            body["deleted"] = vals["deleted"]
            body["marketable"] = vals["marketable"]
            try:
                optionProperty = json.loads(vals['custom_property']).get('optionProperty', [])
                optionProperty.extend(json.loads(vals['custom_property']).get('customProperty', []))
            except Exception as ex:
                optionProperty = []
            if optionProperty:
                for pro in optionProperty:
                    for k, v in pro.iteritems():
                        #NOTE(jake):trun the chinese into pinyin
                        if is_alphabet(k):
                            body[k] = v
                        else:
                            body["".join(pinyin_obj.hanzi2pinyin(k))] = v

            if DEBUG:
                print "mysql_to_es body: ", body

            global CONN
            sql = """
            SELECT sp.city_id, sp.area_id
            FROM goods gs
            LEFT JOIN shop sp ON sp.id=gs.shop_id
            WHERE gs.id=%s;
            """
            city_area_info = fetchone(CONN, sql, (body["id"],))
            if city_area_info:
                body["city_id"] = city_area_info["city_id"]
                body["area_id"] = city_area_info["area_id"]
            return body

        body["name"] = vals['name']
        body['id'] = vals['id']
        body['date'] = vals['create_time'].strftime("%Y-%m-%d %H:%M:%S")
        body['deleted'] = vals['deleted']
        body["sales"] = vals.get("sales", 0)
        body['provider_goods_category_id'] = vals['provider_goods_category_id']
        body["index"] = ["floriation_online_provider_goods", "goods"]

        #商品默认设置审核通过，即检索到该数据！
        body["status"] = "2"
        #FIXME: 销量???
        #body['count'] = "0"

        price = [{"price": -1}]
        if vals['quotation_method'] == "0":
            price = json.loads(vals['quotation_quantity'])['scheme']
        elif vals['quotation_method'] == "1":
            price = json.loads(vals['quotation_standard'])

        price = min(price, key=lambda x: float(x['price']))
        if price:
            price = price['price']
        body['price'] = price

        optionProperty = json.loads(vals['custom_property'])['optionProperty']
        if optionProperty:
            for pro in optionProperty:
                for k, v in pro.iteritems():
                    #NOTE(jake):trun the chinese into pinyin
                    if is_alphabet(k):
                        body[k] = v
                    else:
                        body["".join(pinyin_obj.hanzi2pinyin(k))] = v

        body['marketable'] = vals['marketable']
        return body

    def close(self):
        if self.stream:
            self.stream.close()

def run():
    obj = SyncES()
    try:
        obj._run_new()
    except Exception as ex:
        print "error:", ex.message, ex.args
        LOG.error("sync data to es error. because %s and %s ." % (ex.message, ex.args))
        obj.close()

run()
