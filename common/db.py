
# -*- encoding: utf-8 -*-
#author: hzruandd
#createtime: 2016-01-08
#select gs.id, shop_id, sp.city_id, sp.area_id from goods gs left join shop sp on sp.id=gs.shop_id where gs.id=85;

import MySQLdb
import functools

from DBUtils.PooledDB import PooledDB
from DBUtils.PooledDB import NotSupportedError
from MySQLdb.cursors import DictCursor
from MySQLdb import IntegrityError
from common.log import LOG
from common.conf import DICT_CONF


mysql_conf = {
    "host": DICT_CONF["host"],
    "port": DICT_CONF["port"],
    "user": DICT_CONF["user"],
    "passwd": DICT_CONF["passwd"],
    "db": DICT_CONF["db"],
}


_DB_MANAGER = None


def get_config(cursorclass=DictCursor):
    kwargs = {
        'host': mysql_conf['host'],
        'user': mysql_conf['user'],
        'passwd': mysql_conf['passwd'],
        'db': mysql_conf['db'],
        'port': mysql_conf['port'],
        'use_unicode': True,
        'charset': 'utf8',
        'cursorclass': cursorclass,
    }
    return kwargs


class DataBasePools(object):
    _pools = None

    def __init__(self, creator=MySQLdb, mincached=2,
                 maxcached=5, cursorclass=DictCursor, *args, **kwargs):
        """
        Set up the DB-API 2 connection pool.

        creator: either an arbitrary function returning new DB-API 2
            connection objects or a DB-API 2 compliant database module
        host: database host
        port: database port
        user, passwd, db_name: database user, passwd, db_name
        mincached: initial number of idle connections in the pool
            (0 means no connections are made at startup)
        maxcached: maximum number of idle connections in the pool
            (0 or None means unlimited pool size)
        cursorclass: cursor metaclass, if creator is MySQLdb, default value is
        DictCursor.  At the same time, you can override the cursor metaclass.

        """
        self._conn = self._get_conn(creator, mincached, maxcached,
                                    cursorclass)
        self._cursor = self._conn.cursor()

    def _get_conn(self, creator, mincached, maxcached, cursorclass):
        if self._pools is None:
            kwargs = get_config(cursorclass)
            try:
                self._pools = PooledDB(creator=creator, mincached=mincached,
                                       maxcached=maxcached, **kwargs)
            except NotSupportedError as ex:
                raise NotSupportedError('DB-API module not supported by '
                                        'PooledDB: %s' % ex)
        return self._pools.connection()

    def get_conn(self):
        return self._conn

    @property
    def conn(self):
        return self._conn

#    @property
#    def cursor(self):
#        return self._cursor

    def close_connection(self):
        self._pools.close()

    def __del__(self):
        """Delete the connection."""
        try:
            if self._pools:
                self._pools.close()
        except Exception:
            pass


class MysqlPool(DataBasePools):
    def __init__(self, creator=MySQLdb):
        super(MysqlPool, self).__init__(creator)


def get_db_conn():
    local_mysql = get_config()
    host = local_mysql['host']
    port = local_mysql['port']
    user = local_mysql['user']
    passwd = local_mysql['passwd']
    db = local_mysql['db']
    conn = MySQLdb.connect(host=host, port=port, user=user, passwd=passwd,
                           db=db, charset='utf8', cursorclass=DictCursor)
    return conn


def get_pool_conn():
    try:
        global _DB_MANAGER
        if _DB_MANAGER is None:
            _DB_MANAGER = MysqlPool()
    except Exception as exc:
        raise Exception('cannot create mysql pools: %s' % exc)

    return _DB_MANAGER.conn


def execute(conn=None, sql='', param=None, commit=True):
    if not conn:
        conn = get_db_conn()
    cursor = conn.cursor()
    try:
        if param == None:
            cursor.execute(sql)
        else:
            cursor.execute(sql, param)
    except Exception as ex:
        conn.rollback()

    if commit:
        conn.commit()
    return cursor


def fetchone(conn=None, sql='', param=None):
    cursor = execute(conn, sql, param)
    if cursor:
        res = cursor.fetchone()
    else:
        res = None
#    cursor.close()
#    conn.close()
    return res


def fetchall(conn=None, sql='', param=None):
    cursor = execute(conn, sql, param)
    if cursor:
        res = cursor.fetchall()
    else:
        res = None
#    cursor.close()
#    conn.close()
    return res


def check(func):

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        try:
            func(*args, **kwargs)
        except Exception as ex:
            msg = 'error:ex sql error: %s .' % ex
            LOG.error(msg)
            raise Exception(msg)
    return wrapper


def get_keys_for_sql(conn, tablename, rowdict):
    cursor = execute(conn, "describe %s" % tablename)
    result = cursor.fetchall()
    types = [(row['Field'], row['Type']) for row in result]
    allowed_keys = set(row['Field'] for row in result)
    keys = allowed_keys.intersection(rowdict)
    res = []
    for key in keys:
        for tp in types:
            if key == tp[0]:
                res.append(tp)
    types = set(res)
    if len(rowdict) > len(keys):
        unknown_keys = set(rowdict) - allowed_keys
        LOG.debug("error: skipping keys: %s", ", ".join(unknown_keys))

    return keys, types


def update_by_dict(conn, tablename, rowdict, where):
    '''
    where: such as 'ContantID=1'
    '''
    if not conn:
        conn = get_db_conn()
    keys, types = get_keys_for_sql(conn, tablename, rowdict)
    #NOTE(rdd): filter out those columns which in ["None", "none", None]
    columns = []
    for key in keys:
        if rowdict[key] in ["none", "None", None]:
            rowdict[key] = None
        columns.append(key)

    values = []
    for col in columns:
        for key in types:
            if key[0] == col:
                value = rowdict[key[0]]
                if key[1].endswith("int"):
                    value = int(rowdict[key[0]])
        values.append(value)
    values = tuple(values)

    template = []
    for key in columns:
        template.append("%s=" % key + "%s")
    columns_template = ", ".join(template)

    columns = set(columns)
    columns = ", ".join(columns)

    sql = "UPDATE  %s SET %s WHERE %s" % (tablename, columns_template, where)
    execute(conn, sql, values)
    return where.split("=")[1]


def insert_by_dict(conn, tablename, rowdict, replace=False):
    if not conn:
        conn = get_db_conn()
    keys, types = get_keys_for_sql(conn, tablename, rowdict)
    columns = ", ".join(keys)
    values_template = ", ".join(["%s"] * len(keys))

    if replace:
        sql = "REPLACE INTO %s (%s) VALUES (%s)" % (
            tablename, columns, values_template)
    else:
        sql = "INSERT INTO %s (%s) VALUES (%s)" % (
            tablename, columns, values_template)

    values = []
    for key in keys:
        v = rowdict[key]
        if v in ["", "none", "None"]:
            v = None
        values.append(v)
    return execute(conn, sql, values)


def delete_by_dict(tablename, where):
    # where: "VisitID=2"
    conn = get_db_conn()
    sql = 'DELETE FROM %s WHERE %s' % (tablename, where)
    execute(conn, sql)
    return where.split("=")[1]
