# -*- coding: utf8 -*-
import requests
import json

from settings import DEBUG
from common.conf import DICT_CONF
from common.log import LOG

ES_URI = DICT_CONF["ES_URI"]

def do_request(_action, url, body):
    url = ES_URI + url
    action = getattr(requests, _action.lower())
    result = {"status": 400}
    try:
        if body and _action.lower() == "get":
            response = action(url, json.dumps(body))
        else:
            response = action(url, json.dumps(body))
        res = response.json()
        if DEBUG:
            print res
        data = {}
        if "error" in res:
            status_code = res['status']
            message = res['error']
        elif "hits" in res:
            data.update(res['hits'])
        else:
            data = res

    except Exception as ex:
        LOG.error("do_request data is %s, and the  error is : %s" % (body, ex.message))
        return result
    result = {"status": response.status_code, "message": response.reason,
              "data": data}
    return result


def is_number(uchar):
    """判断一个unicode是否是数字"""
    if uchar >= u'\u0030' and uchar<=u'\u0039':
        return True
    else:
        return False

def is_alphabet(uchar):
    """判断一个unicode是否是英文字母"""
    if (uchar >= u'\u0041' and uchar<=u'\u005a') or (uchar >= u'\u0061' and uchar<=u'\u007a'):
        return True
    else:
        return False
