# encoding: utf-8
# -*- coding: utf-8 -*-

import json
import os
import sys

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
from tornado.options import define, options
from tornadoes import ESConnection

from settings import GOODS_MAPPINGS, BOOST, SUPPORT_FIELDS
from settings import DEBUG
from common.pinyin import pinyin_obj, PinYin
from common.utils import do_request, is_alphabet
from common.log import LOG
from common.conf import DICT_CONF

ESPORT = DICT_CONF["ESPORT"]
ESHOST = DICT_CONF["ESHOST"]

if not pinyin_obj:
    pinyin_obj = PinYin()
    pinyin_obj.load_word()

SEARCH_KEYS = GOODS_MAPPINGS.keys()


define("port", default=8989, help="run on the given port", type=int)
tornado.options.parse_command_line()

import sys
reload(sys)
sys.setdefaultencoding('utf-8')


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [(r"/es", SearchHandler),
                    (r"/interface", InterfaceHandler),
                    (r"/", HomeHandler)]
        settings = dict(
            debug=True,
            template_path=os.path.join(os.path.dirname(__file__), "templates"),
            static_path=os.path.join(os.path.dirname(__file__), "static"),
        )
        tornado.web.Application.__init__(self, handlers, **settings)


class SearchHandler(tornado.web.RequestHandler):
    """
    >>> set and update index
    http://es.xiaoleilu.com/070_Index_Mgmt/10_Settings.html

    自定义分析器
    http://es.xiaoleilu.com/070_Index_Mgmt/20_Custom_Analyzers.html

    Elasticsearch的备份和恢复
    http://keenwon.com/1393.html

    #html_strip
    http://bit.ly/1B6f4Ay

    >>>multi_match:在多个字段上执行 match 查询并一起返回结果,
    在同一个索引的每一个类型中，确保用同样的方式映射同名的字段
    {
    "query": {
    "multi_match": {
    "query": "百 玫瑰 水晶岛",
    "fields": ["jake.name","fake.address"]
            }
        }
    }

    >>>>>>>>>>>>元数据：_all 字段
    一个所有其他字段值的特殊字符串字段。 此字段太屌了啊，可以在文档中所有字段
    进行检索，包括字典套字典的value都可以啊。query_string 在没有指定字段时默认
    用 _all 字段查询.
    GET /_search
    {
    "match": {
    "_all": "玫瑰 百  29  水晶岛"
        }
    }
    """
    es = ESConnection(ESHOST, ESPORT)

    @tornado.web.asynchronous
    def get(self, indice="floriation_online_provider_goods", tipo="goods", _validate=False):
        """
        GET /_search
        {
        "query" : {
        "filtered" : {
        "query":   { "match": { "tweet": "manage text search" }},
        "filter" : { "term" : { "user_id" : 2 }}
        }
        },
        "sort": [
        { "date":   { "order": "desc" }},
        { "_score": { "order": "desc" }}
        ]
        }
        {
        "from": 30,
        "size": 10
        }
        """
        data = json.loads(self.get_argument("data"))
        query = self._simple_query(data)
        tipo = data['type']
        if tipo == "goods":
            indice = "floriation_online_goods"
        if DEBUG:
            print query
        self.search(indice, tipo, query, self.callback)

    def post(self, indice="floriation_online_provider_goods", tipo="goods"):
        data = json.loads(self.get_argument("data"))
#        data = {"category": [29,5], "type": "goods"}
        query = self._simple_query(data)
        tipo = data['type']
        if tipo == "goods":
            indice = "floriation_online_goods"
        if DEBUG:
            print query
        self.search(indice, tipo, query, self.callback)


    def callback(self, response):
        self.content_type = 'application/json'
        result = json.loads(response.body)
        re = {"status": 200, "message": "ok", "data": {}}
        try:
            ids = [int(da["_id"]) for da in result["hits"]["hits"]]
            total = result["hits"]["total"]
        except Exception as ex:
            ids = []
            total = 0
        re["data"]["ids"] = ids
        re["data"]['total'] = total
        if DEBUG:
            print "return back", re
        self.write(re)
        self.finish()

    def delete(self, indice, tipo, _id):
        self.es.delete(indice, tipo, _id)

    def put(self, indice, tipo, _id, body):
        self.es.put(indice, tipo, _id, body)

    def search(self, indice, tipo, body, callback):
        self.es.search(index=indice, type=tipo, source=body, callback=callback)

    def _simple_query(self, params):
        query = {"query":
                 {
                     "bool": {
                         "should": [],
                         "minimum_should_match": 0
                     }
                 },
                 "from": 0, "size": 20,
                 "fields": ["id"]
        }
        must = []
        should = []
        is_goods = True if params.get("type") == "goods" else False
        _type = params.get("category", [])
        if DEBUG:
            # FIXME: delete me
           # _type = 1
           pass

        for tp in  _type:
            if int(tp) in [-1, 0]:
                break
            if is_goods:
                _match = {"match": {"goods_category_id": int(tp)}}
            else:
                _match = {"match": {"provider_goods_category_id": int(tp)}}

            should.append(_match)

        yiji_data = params.get("condition", {})
        price = yiji_data.pop("price", {})
        _range = {}
        if price:
            start = price.get("start", None)
            end = price.get("end", None)
            if not (start in [0, "0", "0.0"] and end in ["9999999", "99999", 9999999, 999999, 99999]):
                _range["range"] = {"price": {}}
                if start:
                    _range["range"]['price']['gte'] = float(start)
                if end:
                    _range["range"]['price']['lte'] = float(end)

        if _range and _range.get("range", {}).get("price", {}).keys():
            should.append(_range)

        #{"category":"0","condition":{"name":"鲜花","price":{"start":100,"end":200}},"orderBy":{"_createtime":"desc"},"pageBounds":{"_size":20,"_from":0},"type":"shop"}

        for k, v in yiji_data.iteritems():
            if is_alphabet(k):
                _match = {"match": {k: v}}
            else:
                _match = {"match": {"".join(pinyin_obj.hanzi2pinyin(k)): v}}

            should.append(_match)

        #add city and area to search
        cityId = params.get("cityId", 0)
        areaId = params.get("areaId", [])
        delete = 0
        if len(areaId) == 1:
            _match = {"match": {"area_id": areaId[0]}}
            must.append(_match)
        else:
            for area in areaId:
                _match = {"match": {"area_id": area}}
                should.append(_match)

        if len(areaId) >= 2:
            delete = 1 - len(areaId)

        #filter out the cityid=0
        if not areaId and int(cityId):
            _match = {"match": {"city_id": cityId}}
            must.append(_match)

        if not must and not should:
            if False:
                _must = {"match": {"status": "2"}}
                must.append(_must)
                query["query"]["bool"]["must"] = must
            if DEBUG:
                print "查询语句：", query
            return query

        #add must condition (status=2), deleted=0, marketable=0 to query
        if True:
            _must = {"match": {"deleted": "0"}}
            must.append(_must)
            _must = {"match": {"marketable": "0"}}
            must.append(_must)
            _must = {"match": {"status": "2"}}
            must.append(_must)

        erji_data = params.get("orderBy", {})
        #{"_price": "desc", "_sales": "desc"}
        #FIXME:为了保持灵活度，建议也按照当时系统添加的汉语属性转化的汉语拼音
        #做为排序的字段，而不是这样(_price, _sales)的转化后字段。
        sort = []
        for k, v in erji_data.iteritems():
            #_sort = {"".join(pinyin_obj.hanzi2pinyin(k)): {"order": v}}
            k = k.replace("_", "")
            if k == "createtime":
                k = "date"
            _sort = {k: {"order": v}}
            sort.append(_sort)

        sanji_data = params.get("pageBounds", {})
        _from = int(sanji_data.get("_from", 0))
        _size = int(sanji_data.get("_size", 20))

        if must:
            query["query"]["bool"]["must"] = must
        if should:
            query["query"]["bool"]["should"] = should
            if _type:
                num = 1 - len(_type)
            else:
                num = 0
            true_items = 0
            if yiji_data:
                true_items = len(yiji_data.keys())
            if true_items:
                #num = true_items + len(should) + num
                num = len(should) + num
                if num <= 0:
                    num = 1
            query["query"]["bool"]["minimum_should_match"] = num + delete
        if sort:
            query["sort"] = sort
        if _size:
            query["from"] = _from
            query['size'] = _size

        query["fields"] = SUPPORT_FIELDS
        if DEBUG:
            LOG.info("query params: %s" % params)
            LOG.info("query: %s" % query)
            print "查询语句：", query

        return query

    def _query(self, params, _all=True):
        if _all:
            values = params.get("values", "鲜花 玫瑰 节")
            return {"query": {"match": {"_all": values}}, "from": 0 ,"size": 20}

        if not params:
            return {}

        query = {
            "query": {
                "bool": {
                }
            }
        }
        must = []
        #NOTE(jake):这里按照用户点击的搜索字段循序作为搜索优先级，即第一个字段
        #作为默认必须包含字段，从而放到must去检索之。
        yiji_data = params.get("yiji", {})
        pro_keys = yiji_data.keys()
        if pro_keys:
            first_key = pro_keys[0]
            _must = {"match": {first_key: {"query": yiji_data[first_key], "boost": BOOST.get(first_key, 3)}}}
            must.append(_must)

        #By default, none of the should clauses are required to match, with
        #one exception: if there are no must clauses, then at least one should clause must match.
        #we can control how many should clauses need to match by using the
        #minimum_should_match parameter, either as an absolute number or as a percentage:
        should = []
        if pro_keys > 1:
            _should = {"bool": {
                "must": [
                ]
            }}
            for k in pro_keys[1:]:
                _match = {"match": {k: yiji_data[k]}}
                _should['bool']['must'].append(_match)

            should.append(_should)

        erji_data = params.get("erji", {})
        #{"_price": "desc", "_sales": "desc"}
        #FIXME:为了保持灵活度，建议也按照当时系统添加的汉语属性转化的汉语拼音
        #做为排序的字段，而不是这样(_price, _sales)的转化后字段。
        sort = []
        for k, v in erji_data.iteritems():
            _sort = {k: {"order": v}}
            sort.append(_sort)

        sanji_data = params.get("sanji", {})
        _from = sanji_data.get("_from", 0)
        _size = sanji_data.get("_size", 20)

        if must:
            query['query']['bool']['must'] = must
        if should:
            query['query']['bool']["should"] = should
        if sort:
            query['query']['sort'] = sort
        if _size:
            query["from"] = _from
            query['size'] = _size

        return query

class InterfaceHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("interface.html")


class HomeHandler(tornado.web.RequestHandler):
    def get(self):
	    self.write("""
             <div style="text-align: center">
             <div style="font-size: 72px">%s</div>
             <div style="font-size: 144px">%s</div>
             </div>""" % ('Hello ', 'Welcome to ES!'))


def get_port():
    try:
        port = sys.argv[1:][0]
        _, port = port.split("port=")
    except Exception as ex:
        port = options.port - 1

    return port

if __name__ == "__main__":
    http_server = tornado.httpserver.HTTPServer(Application())
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()
