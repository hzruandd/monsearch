# encoding: utf-8
DEBUG = True
LOG_PATH = "log/search.log"
LOG_FILE = "log/search.log"


GOODS = ["品牌","品种", "适用对象", "适用节日", "主花材"]


GOODS_MAPPGING = {
    "id": "id",
    "name": "brandname",
    "scene": "scene",
    "object": "object",
    "holiday": "holiday",
    "price": "price",
    "sales": "goods_sales",
    "flower_material": "flower_material",
    "pinpai": "brandname",
    "pinzhong": "brandtype",
    "shiyongduixiang": "object",
    "shiyongjieri": "holiday",
    "zhuhuacai": "flowermaterial",
}
GOODS_MAPPINGS = {
    "id": "id",
    "name": "brandname",
    "scene": "scene",
    "object": "object",
    "holiday": "holiday",
    "price": "price",
    "sales": "sales",
    "pinpai": "pinpai",
    "pinzhong": "pinzhong",
    "shiyongduixiang": "shiyongduixiang",
    "shiyongjieri": "shiyongjieri",
    "zhuhuacai": "zhuhuacai",
    "fenlei": "fenlei",
    "jiage": "jiage",
    "leixin": "leixin",
}
BOOST = {
    "pinpai": 4,
    "leixin": 2,
    "shiyongjieri": 3,
    "jiage": 3,
    "fenlei": 3,
}

SUPPORT_FIELDS = ["id"]
